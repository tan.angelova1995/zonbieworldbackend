package com.example.zonbieworld.controller;

import com.example.zonbieworld.dto.UserDTO;
import com.example.zonbieworld.model.User;
import com.example.zonbieworld.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping("/users")
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/user")
    public UserDTO getUserById(@RequestParam(name="id", required = true) Long userId){
        return userService.getUserById(userId);
    }

    @DeleteMapping("/delUser")
    public ResponseEntity<Long> deleteUserById(@RequestParam(name="id", required = true) Long userId){
        boolean isRemoved = userService.deleteUserById(userId);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(userId, HttpStatus.OK);
    }

    @PostMapping("/createUser")
    // I have to have json file as input
    public ResponseEntity<Long> createUser(@RequestBody User user){

        Long createdId = userService.createUser(user);

        if (createdId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(createdId, HttpStatus.OK);

    }

    @DeleteMapping("/deleteAllUsers")
    public ResponseEntity deleteAllUsers(){
        if (userService.deleteAllUsers()){
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getNbBlocksPerDay")
    public Double getNbBlocksPerDay(@RequestParam(name="id", required = true) Long userId){
        return userService.getNbProteinBlocks(userId);
    }
}
