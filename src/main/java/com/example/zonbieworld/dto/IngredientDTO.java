package com.example.zonbieworld.dto;

public class IngredientDTO {
    private Long ingredientID;
    private String name;
    private Long recipeId;

    public IngredientDTO(){
    }

    public Long getIngredientID(){
        return this.ingredientID;
    }

    public void setIngredientID(Long newID){this.ingredientID=newID;}

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Long getRecipeId(){return this.recipeId;}

    public void setRecipeId(Long newRecipeId){this.recipeId=newRecipeId;}
}
