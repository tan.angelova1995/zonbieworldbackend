package com.example.zonbieworld.dto;

import com.example.zonbieworld.model.Ingredient;
import lombok.Data;

import java.util.List;


@Data
public class RecipeDTO {
    private Long recipeID;
    private String nameRecipe;
    private List<Ingredient> ingredients;
  //  private Map<Ingredient, Quantity> quantityMap;
    private String instructions;
    private Double blocksFat;
    private Double blocksProtein;
    private Double blocksCarbs;

    public RecipeDTO(){}

    public Long getRecipeID(){
        return this.recipeID;
    }

    public void setRecipeID(Long newID){
        this.recipeID = newID;
    }

    public String getNameRecipe(){
        return this.nameRecipe;
    }

    public void setNameRecipe(String newName){
        this.nameRecipe = newName;
    }

    public String getInstructions(){
        return this.instructions;
    }

    public void setInstructions(String instructions){
        this.instructions = instructions;
    }

    public Double getBlocksFat(){
        return this.blocksFat;
    }

    public void setBlocksFat(Double newTotalFat){
        this.blocksFat = newTotalFat;
    }

    public Double getBlocksProtein(){
        return this.blocksProtein;
    }

    public void setBlocksProtein(Double newTotalProtein){
        this.blocksProtein = newTotalProtein;
    }

    public Double getBlocksCarbs(){
        return this.blocksCarbs;
    }

    public void setBlocksCarbs(Double newTotalCarbs){
        this.blocksCarbs = newTotalCarbs;
    }

    public List<Ingredient> getIngredients(){return this.ingredients;}

    public void addIngredient(Ingredient ingredient){this.ingredients.add(ingredient);}

    public void addIngredients(List<Ingredient> ingredients) {this.ingredients=ingredients;}
}
