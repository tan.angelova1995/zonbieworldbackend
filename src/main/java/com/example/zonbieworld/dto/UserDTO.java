package com.example.zonbieworld.dto;

public class UserDTO {
    private long userId;
    private String name;
    private String email;
    private String sex;


    public UserDTO(){}

    public Long getUserId(){
        return this.userId;
    }

    public String getEmail(){
        return this.email;
    }

    public void setUserId(Long newId){
        this.userId = newId;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setName(String first, String last){this.name = first + " " + last;}

    public String getName(){return this.name;}

    public void setSex(String sex){this.sex = sex;}

    public String getSex(){
        return this.sex;
    }


    public void setUserId(long userId) {
        this.userId = userId;
    }
}
