package com.example.zonbieworld.model;

public enum BlockType {
    Carbohydrate,
    Fat,
    Protein,
    CarbohydrateFatProtein
}
