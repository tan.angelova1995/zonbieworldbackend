package com.example.zonbieworld.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;

import lombok.Setter;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ingredients")
@Getter
@Setter
@Lazy
public class Ingredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ingredientID;
    @ManyToOne(
            //fetch = FetchType.LAZY,
            //optional = false

    )
    @JsonBackReference
    @JoinColumn(
            name = "food_id",
            //   referencedColumnName = "recipe_id",
            //  nullable = false,
            foreignKey = @ForeignKey(
                    name = "fk_ingredient_food_id"
            )
    )
    private Food food;
    @Column(name = "quantity")
    private Double quantity;
    @Column(name = "unitFood")
    private UnitFood unitFood;
    @ManyToOne(
            //fetch = FetchType.LAZY,
            //optional = false

    )
    @JsonBackReference
   @JoinColumn(
            name = "recipe_id",
         //   referencedColumnName = "recipe_id",
          //  nullable = false,
            foreignKey = @ForeignKey(
                    name = "fk_ingredients_recipes_id"
            )
    )
    private Recipe recipe;

    public Long getIngredientID() {
        return ingredientID;
    }

    public void setIngredientID(Long ingredientID) {
        this.ingredientID = ingredientID;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Ingredient(){}

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public UnitFood getUnitFood() {
        return unitFood;
    }

    public void setUnitFood(UnitFood unitFood) {
        this.unitFood = unitFood;
    }
}
