package com.example.zonbieworld.model;

public class Quantity {
    private Double quantity;
    private UnitFood mertics;

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public UnitFood getMertics() {
        return mertics;
    }

    public void setMertics(UnitFood mertics) {
        this.mertics = mertics;
    }
}
