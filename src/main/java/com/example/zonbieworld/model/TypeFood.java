package com.example.zonbieworld.model;

public enum TypeFood {
    cooked_vegetables,
    fruit_juices,
    fruits,
    grains_cereals_breads,
    grains,
    raw_vegetables,
    oils_nuts_spreads,
    protein_rich_dairy,
    alcohol,
    eggs,
    fish_seafood,
    mixed_protein_sources,
    protein_carbohydrate_sources,
    meat_poultry
}
