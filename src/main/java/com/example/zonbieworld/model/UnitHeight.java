package com.example.zonbieworld.model;

public enum UnitHeight {
    cm,
    inches
}
