package com.example.zonbieworld.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name="email")
    private String email;
    @Column(name="firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name="username")
    private String username;
    @Column(name="password")
    private String password;
    @Column(name="age")
    private Integer age;
    @Column(name="sex")
    private Sex sex;
    @Column(name="height")
    private double height; // inches
    @Column(name="weight")
    private double weight; //pounds
    @Column(name="BMI")
    private double BMI;
    @Column(name="nbBlockPerDay")
    private Double nbBlockPerDay;
    @Lob
    private byte[] profilePicture;
    @Column(name="bodyFatPercentage")
    private Double bodyFatPercentage;
    @Column(name="activityLevel")
    private Double activityLevel;
    @Temporal(TemporalType.DATE)
    private Date date = new Date();

    public User(){}

    public Long getId(){ return this.id; }

    public String getEmail(){
        return this.email;
    }

    public void setFirstName(String newName){
        this.firstName = newName;
    }

    public String getFirstName(){return this.firstName;}

    public String getLastName(){return this.lastName;}

    public void setLastName(String newName){
        this.lastName = newName;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public Integer getAge(){return this.age;}

    public void setAge(Integer newAge){this.age = newAge;}

    public String getSex(){return this.sex.toString();}

    public void setSex(String sex){this.sex = Sex.valueOf(sex);}

    public Double getWeight(){return this.weight;}

    public void setWeight(Double measurement, UnitWeight unit){
        if (unit.equals(UnitWeight.kg)){
            this.weight=measurement*2.204623;
        } else {
            this.weight = measurement;
        }
    }

    public Double getHeight(){return this.height;}

    public void setHeight(Double measurement, UnitHeight unit){
        if (unit.equals(UnitHeight.cm)){
            this.height=measurement*0.3937008;
        } else {
            this.height = measurement;
        }
    }

    public Double getBMI(){
        return this.BMI;
    }

    public void setBMI(){
        try{
            this.BMI = this.weight*(this.height*this.height)*703;
        }catch (NullPointerException e){
            System.out.println("Please, provide weight and height information");
        }
    }

    public Double getBodyFatPercentage(){
        return this.bodyFatPercentage;
    }

    public void setBodyFatPercentage(){
        if (this.sex.equals(Sex.Female)){
            this.bodyFatPercentage = (1.20*this.BMI) + (0.23*this.age) - 5.4;
        } else {
            this.bodyFatPercentage = (1.20*this.BMI) + (0.23*this.age) - 16.2;
        }
    }

    public void setActivityLevel(Double activityLevel){
        this.activityLevel = activityLevel;
    }

    public Double findNbBlocksPerDay(){
        setBMI();
        setBodyFatPercentage();
        this.nbBlockPerDay = (this.activityLevel*(this.weight-this.bodyFatPercentage*this.weight))/7;
        if (this.nbBlockPerDay<11){
            this.nbBlockPerDay= 11.0;
        }
        return this.nbBlockPerDay;
    }


    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Double getNbBlockPerDay() {
        return nbBlockPerDay;
    }

    public void setNbBlockPerDay(Double nbBlockPerDay) {
        this.nbBlockPerDay = nbBlockPerDay;
    }

    public byte[] getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setBodyFatPercentage(Double bodyFatPercentage) {
        this.bodyFatPercentage = bodyFatPercentage;
    }

    public Double getActivityLevel() {
        return activityLevel;
    }
}
