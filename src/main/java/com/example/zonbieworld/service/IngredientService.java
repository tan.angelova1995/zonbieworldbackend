package com.example.zonbieworld.service;

import com.example.zonbieworld.dto.IngredientDTO;
import com.example.zonbieworld.model.Ingredient;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface IngredientService {
    public List<IngredientDTO> getAllIngredients();
    public IngredientDTO getIngredientById(Long ingredientId);
    public Boolean deleteIngredientById(Long ingredientId);
    public Long createIngredient(Ingredient ingredient);
    public IngredientDTO convertEntityToDto(Ingredient ingredient);
    public Boolean deleteAllIngredients();
}
