package com.example.zonbieworld.service;

import com.example.zonbieworld.dto.RecipeDTO;
import com.example.zonbieworld.model.Ingredient;
import com.example.zonbieworld.model.Recipe;
import com.example.zonbieworld.repository.IngredientRepository;
import com.example.zonbieworld.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RecipeServiceImpl implements RecipeService{

    @Autowired
    RecipeRepository recipeRepository;

    @Autowired
    IngredientRepository ingredientRepository;

    @Override
    public List<RecipeDTO> getAllRecipes(){
        return this.recipeRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }


    @Override
    public Recipe getRecipeById(Long recipeId){
        Optional<Recipe> optinalEntity =  recipeRepository.findById(recipeId);
        Recipe recipeEntity = optinalEntity.get();
        return recipeEntity;
    }

    @Override
    public RecipeDTO getRecipeDTOById(Long recipeId){
        Optional<Recipe> optinalEntity =  recipeRepository.findById(recipeId);
        Recipe recipeEntity = optinalEntity.get();
        return this.convertEntityToDto(recipeEntity);
    }
    @Override
    public Boolean deleteRecipeById(Long recipeId){
        try {
            Optional<Recipe> optionalEntity = recipeRepository.findById(recipeId);
            if (optionalEntity.isPresent()){
                Recipe recipeEntity = optionalEntity.get();
                recipeRepository.delete(recipeEntity);
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e){
            return false;
        }
    }

    @Override
    public Long createRecipe(@RequestBody Recipe recipe){
            recipeRepository.save(recipe);
            return recipe.getRecipeId();
    }

    @Override
    public RecipeDTO convertEntityToDto(Recipe recipe){
        RecipeDTO recipeDTO = new RecipeDTO();
        recipeDTO.setRecipeID(recipe.getRecipeId());
        recipeDTO.setNameRecipe(recipe.getNameRecipe());
        recipeDTO.setInstructions(recipe.getInstructions());
        recipeDTO.addIngredients(recipe.getIngredients());
        recipeDTO.setBlocksFat(recipe.getBlocksFat());
        recipeDTO.setBlocksCarbs(recipe.getBlocksCarbs());
        recipeDTO.setBlocksProtein(recipe.getBlocksProtein());
        return recipeDTO;
    }
    @Override
    public Boolean deleteAllRecipes(){
        try{
            recipeRepository.deleteAll();
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public Boolean addIngredientToRecipe(Long recipeId, Long ingredientId){
        Ingredient  newIng  = ingredientRepository
                .findById(ingredientId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "ingredientId " + ingredientId + " not found")
                );

        Optional<Recipe> optionalRecipe = recipeRepository.findById(recipeId);
        if (optionalRecipe.isPresent()){
            Recipe newRec = optionalRecipe.get();
            System.out.println(newRec);
            newRec.addIngredient(newIng);
            System.out.println(newRec.getIngredients());
            Recipe savedRecipe = recipeRepository.save(newRec);
            System.out.println(savedRecipe);
            return true;
        }
        return false;
    }
}
