package com.example.zonbieworld.service;

import com.example.zonbieworld.dto.UserDTO;
import com.example.zonbieworld.model.User;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public interface UserService {
    public List<UserDTO> getAllUsers();
    public UserDTO getUserById(Long userId);
    public Boolean deleteUserById(Long userId);
    public Long createUser(User user);
    public UserDTO convertEntityToDto(User user);
    public Boolean deleteAllUsers();
    public Double getNbProteinBlocks(Long userId);
}
