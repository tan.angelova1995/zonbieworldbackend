package com.example.zonbieworld.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Food.class)
public abstract class Food_ {

	public static volatile SingularAttribute<Food, TypeFood> typeFood;
	public static volatile SingularAttribute<Food, FoodQuality> foodQuality;
	public static volatile SingularAttribute<Food, UnitFood> unitFood;
	public static volatile SingularAttribute<Food, Double> quantityPerBlock;
	public static volatile SingularAttribute<Food, Long> foodID;
	public static volatile SingularAttribute<Food, String> nameFood;
	public static volatile SingularAttribute<Food, BlockType> blockType;

	public static final String TYPE_FOOD = "typeFood";
	public static final String FOOD_QUALITY = "foodQuality";
	public static final String UNIT_FOOD = "unitFood";
	public static final String QUANTITY_PER_BLOCK = "quantityPerBlock";
	public static final String FOOD_ID = "foodID";
	public static final String NAME_FOOD = "nameFood";
	public static final String BLOCK_TYPE = "blockType";

}

