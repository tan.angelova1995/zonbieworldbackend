package com.example.zonbieworld.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ingredient.class)
public abstract class Ingredient_ {

	public static volatile SingularAttribute<Ingredient, Long> ingredientID;
	public static volatile SingularAttribute<Ingredient, Double> quantity;
	public static volatile SingularAttribute<Ingredient, UnitFood> unitFood;
	public static volatile SingularAttribute<Ingredient, Recipe> recipe;
	public static volatile SingularAttribute<Ingredient, Food> food;

	public static final String INGREDIENT_ID = "ingredientID";
	public static final String QUANTITY = "quantity";
	public static final String UNIT_FOOD = "unitFood";
	public static final String RECIPE = "recipe";
	public static final String FOOD = "food";

}

