package com.example.zonbieworld.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Recipe.class)
public abstract class Recipe_ {

	public static volatile SingularAttribute<Recipe, String> instructions;
	public static volatile SingularAttribute<Recipe, Double> blocksCarbs;
	public static volatile SingularAttribute<Recipe, String> nameRecipe;
	public static volatile SingularAttribute<Recipe, Double> blocksProtein;
	public static volatile ListAttribute<Recipe, Ingredient> ingredients;
	public static volatile SingularAttribute<Recipe, Double> blocksFat;
	public static volatile SingularAttribute<Recipe, Long> recipeID;

	public static final String INSTRUCTIONS = "instructions";
	public static final String BLOCKS_CARBS = "blocksCarbs";
	public static final String NAME_RECIPE = "nameRecipe";
	public static final String BLOCKS_PROTEIN = "blocksProtein";
	public static final String INGREDIENTS = "ingredients";
	public static final String BLOCKS_FAT = "blocksFat";
	public static final String RECIPE_ID = "recipeID";

}

