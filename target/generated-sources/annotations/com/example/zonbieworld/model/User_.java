package com.example.zonbieworld.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, Date> date;
	public static volatile SingularAttribute<User, String> lastName;
	public static volatile SingularAttribute<User, Double> bodyFatPercentage;
	public static volatile SingularAttribute<User, Sex> sex;
	public static volatile SingularAttribute<User, Double> weight;
	public static volatile SingularAttribute<User, String> firstName;
	public static volatile SingularAttribute<User, byte[]> profilePicture;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Double> nbBlockPerDay;
	public static volatile SingularAttribute<User, Long> id;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SingularAttribute<User, Integer> age;
	public static volatile SingularAttribute<User, Double> activityLevel;
	public static volatile SingularAttribute<User, String> username;
	public static volatile SingularAttribute<User, Double> height;
	public static volatile SingularAttribute<User, Double> BMI;

	public static final String DATE = "date";
	public static final String LAST_NAME = "lastName";
	public static final String BODY_FAT_PERCENTAGE = "bodyFatPercentage";
	public static final String SEX = "sex";
	public static final String WEIGHT = "weight";
	public static final String FIRST_NAME = "firstName";
	public static final String PROFILE_PICTURE = "profilePicture";
	public static final String PASSWORD = "password";
	public static final String NB_BLOCK_PER_DAY = "nbBlockPerDay";
	public static final String ID = "id";
	public static final String EMAIL = "email";
	public static final String AGE = "age";
	public static final String ACTIVITY_LEVEL = "activityLevel";
	public static final String USERNAME = "username";
	public static final String HEIGHT = "height";
	public static final String B_MI = "BMI";

}

